<?php
 
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Users;


class SignInController extends Controller 
{
    public function signUp(Request $request){
      $user_data = json_decode($request->user_data,true);
      $user = Users::where('email',$user_data['email'])->first();
      if($user){
        return response()->json([
          "status" => "exists"
        ]);
      }
      Users::create([
        'fullname'  =>  strtoupper($user_data['fullname']),
        'mobile_no' =>  $user_data['mobile_no'],
        'email'     =>  $user_data['email'],
        'dob'       =>  $user_data['dob'],
        'password'  =>  Hash::make($user_data['password'])
      ]);
      return response()->json([
        "status" => "success"
      ]);
    }

    public function signin(Request $request){
      $data = [];
      $user = Users::where('email',$request->username)->first();
      if($user){
        if(Hash::check($request->password,$user->password)){
          session_start();
          $data['status'] = "success";
          $_SESSION['uid'] = $user->id;
          $_SESSION['fullname'] = $user->fullname;
          $data['fullname'] = $user->fullname;
          $data['sid'] = session_id();
        }else{
          $data['status'] = "fail";
        }
      }else{
        $data['status'] = "fail";
      }

      return response()->json($data);
    }
}
